package main

import (
	"fmt"
	"os"
	"path/filepath"
)
const (
	cornerGraphSymbol = "└───"
	halfCrossGraphSymbol = "├───"
	levelUpInside string = "|   "
	levelUpOutside string = "    "
)

func perror(err error) {
	fmt.Printf("%s [%s] \n", os.Args[0], err)
	os.Exit(1)
}

func GetDirRow(levelPrefix string, file os.FileInfo) string {
	return levelPrefix + fmt.Sprintf("%s", file.Name())
}
func GetFileRow(levelPrefix string, file os.FileInfo) string {
	size :=  file.Size()
	strSize := fmt.Sprintf("%v%s", size, "b")
	if size == 0 {
		strSize = "Empty"
	}
	return levelPrefix + fmt.Sprintf("%s (%s)", file.Name(), strSize)
}

func LsTree(
	fileInfo []os.FileInfo,
	rootPath string,
	levelPrefix string,
	level int,
	) error {
		var path string
		graphSymbol := halfCrossGraphSymbol

		for pos, file := range fileInfo {
			graphSymbol = cornerGraphSymbol
			if pos < len(fileInfo) - 1 {
				graphSymbol = halfCrossGraphSymbol
			}
			if file.IsDir() {
				fmt.Println(GetDirRow(levelPrefix + graphSymbol, file))

				path = filepath.Join(rootPath, file.Name())
				file, err := os.Open(path)
				if err != nil {
					//fmt.Println(err)
					continue
				}
				defer func() {
					file.Close()
				}()

				dirInfo, err := file.Readdir(-1)
				if err != nil {
					//fmt.Println(err)
					continue
				}

				prefix := ""
				if pos < len(fileInfo) - 1 {
					prefix = levelPrefix + levelUpInside
				} else {
					prefix = levelPrefix + levelUpOutside
				}
				LsTree(dirInfo, path, prefix, level + 1)
			} else {
				fmt.Println(GetFileRow(levelPrefix + graphSymbol, file))
			}
		}
		return nil
}

func dirTree(path string, printFiles bool) error {
	file, err := os.Open(path)
	if err != nil {
		perror(err)
	}
	defer file.Close()

	fileInfo, err := file.Readdir(-1)
	if err != nil {
		return err
	}
	LsTree(fileInfo, path, "", 0)

	return nil
}

func main() {
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		fmt.Printf("%s\n", "usage: mtree <path>")
		os.Exit(1)
	}
	err := dirTree(os.Args[1], true)
	if err != nil {
		perror(err)
	}
}
